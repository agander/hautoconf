{-# LANGUAGE OverloadedStrings #-}

module Main where

import Data.Version
import System.Environment (getArgs, getProgName)
import Data.Char (isDigit, digitToInt)

m4_version :: [Int]
m4_version = map digitToInt $ filter isDigit "0.0.6"

version :: Version
version = makeVersion m4_version

insomma :: Version -> String-> String
insomma version str = str ++ " " ++ showVersion version

main :: IO ()
main = do
  args <- getArgs
  exe <- getProgName
  putStrLn (exe ++ ": ")
  print $ insomma version exe

